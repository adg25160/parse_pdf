﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ParsePdf.aspx.cs" Inherits="Parse.ParsePdf" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="開始解析" />
            <asp:TextBox ID="TxtS1" runat="server"></asp:TextBox>
            <asp:TextBox ID="TxtS2" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="LblRes" runat="server" Text="執行結果："></asp:Label>
            <br />
            <asp:TextBox ID="TxtRes" runat="server" Height="161px" TextMode="MultiLine" Width="379px"></asp:TextBox>
            <br />
            <asp:Label ID="LblParse" runat="server" Text="解析結果"></asp:Label>
            <br />
            <asp:TextBox ID="TxtParse" runat="server" Height="102px" TextMode="MultiLine" Width="379px"></asp:TextBox>
            <br />
        </div>
    </form>
</body>
</html>
